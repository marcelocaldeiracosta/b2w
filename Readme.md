<H1>Solução teste para B2W</H1>


Foram utilizadas as seguintes tecnologias:

<ul>
<li><b>Java:</b> Utilizei o java pois o cargo solicita conhecimentos na liguagem java. Poderia ter feito também com NodeJs.</li>
</ul>

<ul>
<li><b>Spring Boot:</b> Foi utilizado para agilizar a criação do REST para a requisição GET.</li>
</ul>
<ul>
<li><b>Maven:</b> Gerenciador de depedências que estou mais habituado a utilizar e que facilita muito o controle dos pacotes necessários.</li>
</ul>

<h6>Funcionalidade</h6>

Após executar a aplicação está disponível o caminho http://localhost:8080/api/search?texto=

Onde será possível informar um texto para que seja pesquisado nos arquivos disponibilizados para este teste e que estão armazenados na pasta resources.
 
 
<p>
<p><b>Autor:</b> Marcelo Caldeira</p>
<p><b>email:</b> <a href="mailto:marcelocaldeira@gmail.cm">marcelocaldeira@gmail.com</a></p>
<p><b>Telefone:</b> 48 98815-6686</p>
