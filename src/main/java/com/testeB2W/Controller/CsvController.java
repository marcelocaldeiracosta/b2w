package com.testeB2W.Controller;

import com.testeB2W.Business.CsvBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;


@RestController
public class CsvController {
    @Autowired
    private CsvBusiness csvBusiness;

    @GetMapping(value = "/api/healthcheck")
    public ResponseEntity<?> healthCheck() {
        return ResponseEntity.ok("OK!");
    }

    @GetMapping("/api/search")
    public ResponseEntity<?> uploadFile(@RequestParam("texto") String texto) {
        if (texto.isEmpty()) {
            return new ResponseEntity("Informe um texto para pesquisar!", HttpStatus.BAD_REQUEST);
        }

        try {
            return ResponseEntity.ok(csvBusiness.readCsvFile(texto));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


    }

}
