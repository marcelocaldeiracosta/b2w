package com.testeB2W.Business;

import com.testeB2W.DTO.ResponseDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.stream.Stream;


@Component("CvsBusiness")
public class CsvBusiness {

    public ResponseDTO readCsvFile(String texto) throws IOException {
        int quantidade = 0;

        Instant inicio = Instant.now();

        File folder = new File(getClass().getClassLoader().getResource("csv").getPath());
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            try (Stream<String> paths = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.ISO_8859_1)) {
                quantidade += quantidade + paths.filter(lines -> lines.contains(texto)).count();
            }
        }
        Instant fim = Instant.now();
        long tempo = Duration.between(inicio, fim).toMillis();
        ResponseDTO dto = new ResponseDTO();
        dto.quantidade = quantidade;
        dto.milissegundos = tempo;
        return dto;

    }
}
